const router = require('express').Router();

const getNewToken = () => {
    const crypto = require('crypto');
    return crypto.randomBytes(32).toString('hex');
};

router.post('/login', (req, res) => {
    res.json({
        user: req.body,
        token: getNewToken()
    });
});

module.exports = router;
